// import { Base64 } from 'js-base64';
var helper = {
    base64encode(str) {
        return Base64.encode(str);
    },
    base64decode(str) {
        return Base64.decode(str);
    },
    friendlyTime: function (time) {
        time = parseInt(new Date(time).valueOf() / 1000);
        var now = parseInt(Date.parse(new Date()) / 1000);
        if (now - time <= 60) {
            return 'a moment ago';
        } else if (now - time > 60 && now - time <= 3600) {
            return parseInt((now - time) / 60) + ' minutes ago'
        } else if (now - time > 3600 && now - time <= 86400) {
            return parseInt((now - time) / 3600) + ' hours ago'
        } else if (now - time > 86400 && now - time <= 86400 * 7) {
            return parseInt((now - time) / 86400) + ' days ago'
        } else if (now - time > 86400 * 7 && now - time <= 86400 * 30) {
            return parseInt((now - time) / 86400 / 7) + ' weeks ago'
        } else if (now - time > 86400 * 30 && now - time <= 86400 * 30 * 12) {
            return parseInt((now - time) / 86400 / 30) + ' months ago'
        } else {
            return parseInt((now - time) / 86400 / 365) + ' years ago'
        }
    },
    getUserInfo() {
        let userInfo = localStorage.getItem('userInfo') || "{}";
        userInfo = JSON.parse(userInfo);
        return userInfo || false;
    },
    checkImage(url) {
        if (url == 'https://gitee.com/assets/no_portrait.png') {
            return require("~/assets/img/nohead.jpg");
        } else {
            return url;
        }
    }
};
export default helper;