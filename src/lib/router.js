
import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

// 解决ElementUI导航栏中的vue-router在3.0版本以上重复点菜单报错问题
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err)
}

let router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/login',
            component: () => import("@/Login")
        },
        {
            path: '/',
            component: () => import("@/Common"),
            children: [
                {
                    path: 'activity',
                    component: () => import("@/pages/Activity"),
                },
                {
                    path: 'repo',
                    component: () => import("@/pages/Repo"),
                },
                {
                    path: 'issue',
                    component: () => import("@/pages/Issue"),
                },
                {
                    path: 'profile',
                    component: () => import("@/pages/Profile"),
                },
                {
                    path: '',
                    component: () => import("@/pages/Home"),
                },
                {
                    path: '*/*',
                    component: () => import("@/pages/RepoDetail"),
                    children: [
                        {
                            path: 'star',
                            component: () => import("@/pages/MemberStar"),
                        },
                        {
                            path: 'fork',
                            component: () => import("@/pages/MemberFork"),
                        },
                        {
                            path: 'watch',
                            component: () => import("@/pages/MemberWatch"),
                        },
                        {
                            path: 'ide',
                            component: () => import("@/pages/NotFound"),
                        },
                        {
                            path: 'commit',
                            component: () => import("@/pages/NotFound"),
                        },
                        {
                            path: 'issue',
                            component: () => import("@/pages/NotFound"),
                        },
                        {
                            path: 'setting',
                            component: () => import("@/pages/NotFound"),
                        },
                        {
                            path: '',
                            component: () => import("@/pages/ReadMe"),
                        },
                    ]
                },
                {
                    path: '*',
                    component: () => import("@/pages/NotFound"),
                }
            ]
        }
    ]
});

export default router;