import axios from 'axios';
function request(_data) {
    let that = window.app;
    let apiUrl = "https://gitee.com/api/v5/";
    let access_token = localStorage.getItem('access_token') || '';
    const HTTP_STATUS = {
        UNAUTHORIZED: 401,
        OK: 200,
        REDIRECT_FOREVERY: 301,
        REDIRECT: 302,
        BAD_REQUEST: 400,
        FORBIDDEN: 403,
        NOT_FOUND: 404,
        METHOD_UNSURPPORTED: 405,
        SERVER_ERROR: 500,
        GATEWAY_ERROR: 502,
        SERVICE_BUSY: 503,
    };
    let baseData = {
        access_token: access_token,
    };
    let sendData = {};
    if (!_data.method) {
        _data.method = "GET";
    }
    _data.method = _data.method.toUpperCase();
    let disableAlert = true;
    switch (_data.method) {
        case 'PATCH':
            _data.url = _data.url || "";
            if (_data.url.indexOf("https://") < 0 && _data.url.indexOf("http://") < 0) {
                _data.url = apiUrl + _data.url;
            }
            sendData = _data.data;
            if (!_data.noToken) {
                //需要自动加上token
                Object.assign(sendData, baseData);
            }
            axios.patch(_data.url, sendData, {
                headers: {
                    'Content-Type': 'application/json'
                }
            })
                .then(function (response) {
                    if (_data.success) {
                        _data.success(response.data);
                    } else {
                        that.$message.success("操作成功");
                    }
                    _data.final && _data.final();
                })
                .catch(function (error) {
                    if (error.response) {
                        switch (error.response.status) {
                            case HTTP_STATUS.UNAUTHORIZED:
                                that.$router.push("/login");
                                return;
                                break;
                            default:
                        }
                    }
                    if (_data.error) {
                        disableAlert = _data.error(error);
                    } 
                    if(!disableAlert){
                        that.$message.error("请求失败");
                    }
                    _data.final && _data.final();
                });
            break;
        case 'POST':
            _data.url = _data.url || "";
            if (_data.url.indexOf("https://") < 0 && _data.url.indexOf("http://") < 0) {
                _data.url = apiUrl + _data.url;
            }
            sendData = _data.data;
            if (!_data.noToken) {
                //需要自动加上token
                Object.assign(sendData, baseData);
            }
            axios.post(_data.url, sendData, {
                headers: {
                    'Content-Type': 'application/json'
                }
            })
                .then(function (response) {
                    if (_data.success) {
                        _data.success(response.data);
                    } else {
                        that.$message.success("操作成功");
                    }
                    _data.final && _data.final();
                })
                .catch(function (error) {
                    if (error.response) {
                        switch (error.response.status) {
                            case HTTP_STATUS.UNAUTHORIZED:
                                that.$router.push("/login");
                                return;
                                break;
                            default:
                        }
                    }
                    if (_data.error) {
                        disableAlert = _data.error(error);
                    } 
                    if(!disableAlert){
                        that.$message.error("请求失败");
                    }
                    _data.final && _data.final();
                });
            break;
        default:
            _data.url = _data.url || "";
            if (_data.url.indexOf("https://") < 0 && _data.url.indexOf("http://") < 0) {
                _data.url = apiUrl + _data.url;
            }
            if (!_data.noToken) {
                //需要自动加上token
                if (_data.url.indexOf("?") < 0) {
                    _data.url += "?access_token=" + access_token;
                } else {
                    _data.url += "&access_token=" + access_token;
                }
            }
            axios.get(_data.url, {
                headers: {
                    'Content-Type': 'application/json'
                }
            })
                .then(function (response) {
                    if (_data.success) {
                        _data.success(response.data);
                    } else {
                        that.$message.success("操作成功");
                    }
                    _data.final && _data.final();
                })
                .catch(function (error) {
                    if (error.response) {
                        switch (error.response.status) {
                            case HTTP_STATUS.UNAUTHORIZED:
                                that.$router.push("/login");
                                return;
                                break;
                            default:
                        }
                    }
                    if (_data.error) {
                        disableAlert = _data.error(error);
                    } 
                    if(!disableAlert){
                        that.$message.error("请求失败");
                    }
                    _data.final && _data.final();
                });
    }
}
export default request;