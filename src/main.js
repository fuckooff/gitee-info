import Vue from 'vue';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);


Vue.prototype.global = {
    userInfo: false
};


import helper from "./lib/helper";
Vue.prototype.helper = helper;

//主体
import App from './App.vue';


// import clipboard from 'clipboard';
// Vue.prototype.clipboard = clipboard;
import preview from 'vue-photo-preview'
import 'vue-photo-preview/dist/skin.css'
Vue.use(preview)

import router from './lib/router';

//new Vue 启动
window.app = new Vue({
    el: '#app',
    //让vue知道我们的路由规则
    router: router, //可以简写router
    render: c => c(App),
});