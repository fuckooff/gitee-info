<p align="left">
<h1>GiteeInfo</h1>
</p>

### 介绍

一个基于GiteeOpenAPI实现的小工具，可以快速查看用户资料和代码仓库，像在IDE里一样丝滑的预览开源代码。QQ群1140258698

体验一下：<a href="https://gitee.info/" target="_blank">https://Gitee.Info</a>

### 免责声明

所有数据来源于Gitee OpenAPI，数据版权归Gitee所有，本项目仅供学习交流使用，请勿用于商业用途。

### 使用说明

1. clone当前项目 ```git clone https://gitee.com/open-gitee/gitee-info.git```

2. 安装依赖项 ```npm install```

3. 开发环境运行```npm run dev``` 即可预览项目

4. 打包部署生产```npm run build```

### 已实现功能
```
1、用户主页与组织主页信息显示
2、显示用户的开源仓库代码
3、显示仓库代码分支,文件列表
4、显示文件内容与编辑(暂不支持提交)
```


### 参与贡献
```
1. Fork 本仓库
2. 新建分支 添加或修改功能
3. 提交代码
4. 新建 Pull Request
```

